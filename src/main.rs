use lettre::smtp::authentication::{Credentials, Mechanism};
use lettre::smtp::ConnectionReuseParameters;
use lettre::{SmtpClient, Transport};
use lettre_email::EmailBuilder;
use rand::seq::{IteratorRandom, SliceRandom};
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt;
use std::iter;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let families = &[
        Family {
            name: "Parents 1",
            email: "email@example.com",
            children: &["Child 1", "Child 2", "Child 3"],
        },
    ];

    let mut parent_attributions = random_parent_attributions(families);
    let mut child_attributions = random_child_attributions(families);

    while max_same_family_ratio(families, &parent_attributions, &child_attributions) > 0.5 {
        parent_attributions = random_parent_attributions(families);
        child_attributions = random_child_attributions(families);
    }

    send_emails(families, parent_attributions, child_attributions)
}

#[derive(Debug, PartialEq, Hash, Eq)]
struct ComplementaryNameList<'a>(&'a [&'a str]);

impl fmt::Display for ComplementaryNameList<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut comma_separated = String::new();

        let len = self.0.len();

        if len > 0 {
            comma_separated.push_str(" ainsi que ");
        }

        if len > 2 {
            for name in &self.0[0..len - 2] {
                comma_separated.push_str(&name.to_string());
                comma_separated.push_str(", ");
            }
        }

        if len > 1 {
            comma_separated.push_str(&self.0[len - 2].to_string());
            comma_separated.push_str(" et ");
        }

        if len > 0 {
            comma_separated.push_str(&self.0[len - 1].to_string());
        }
        write!(f, "{}", comma_separated)
    }
}

#[derive(Debug, PartialEq, Hash, Eq)]
struct Family<'a> {
    name: &'a str,
    email: &'a str,
    children: &'a [&'a str],
}

fn max_same_family_ratio<'a>(
    families: &[Family<'a>],
    adults: &HashMap<&Family<'a>, &Family>,
    children: &HashMap<&Family<'a>, Vec<&str>>,
) -> f64 {
    families
        .iter()
        .map(|family| same_family_ratio(families, adults[family], &children[family]))
        .max_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal))
        .unwrap_or(0.)
}

fn same_family_ratio(families: &[Family], adults: &Family, children: &[&str]) -> f64 {
    if children.len() == 0 {
        return 0.;
    }

    *iter::once(adults)
        .chain(children.iter().map(|child| {
            families
                .iter()
                .find(|family| family.children.contains(child))
                .unwrap()
        }))
        .fold(HashMap::new(), |mut map, family| {
            map.insert(family, *map.get(family).unwrap_or(&0) + 1);
            map
        })
        .values()
        .max()
        .unwrap_or(&0) as f64
        / (children.len() + 1) as f64
}

fn random_parent_attributions<'a>(
    families: &'a [Family],
) -> HashMap<&'a Family<'a>, &'a Family<'a>> {
    let mut rng = rand::thread_rng();

    let mut targets: Vec<_> = families.iter().collect();

    while families
        .iter()
        .zip(targets.iter())
        .any(|(from, &to)| from == to)
    {
        targets.shuffle(&mut rng);
    }

    families.iter().zip(targets.into_iter()).collect()
}

fn random_child_attributions<'a>(families: &'a [Family]) -> HashMap<&'a Family<'a>, Vec<&'a str>> {
    let mut rng = rand::thread_rng();

    let mut child_attributions: HashMap<_, _> =
        families.iter().map(|family| (family, Vec::new())).collect();

    let mut children: Vec<_> = families
        .iter()
        .flat_map(|family| iter::repeat(family).zip(family.children))
        .collect();
    children.shuffle(&mut rng);

    for (child_family, child) in children {
        attribute_child(&mut child_attributions, families, child_family, child);
    }

    child_attributions
}

fn attribute_child<'a>(
    attributions: &mut HashMap<&Family<'a>, Vec<&'a str>>,
    families: &'a [Family],
    child_family: &Family<'a>,
    child: &'a str,
) {
    let mut rng = rand::thread_rng();

    let target = families
        .iter()
        .filter(|&family| family != child_family)
        .filter(|family| family.children.len() != attributions[family].len())
        .choose(&mut rng);

    match target {
        Some(family) => attributions.get_mut(family).unwrap().push(child),
        None => {
            let target = families
                .iter()
                .filter(|&family| family != child_family)
                .filter(|family| {
                    attributions[family]
                        .iter()
                        .any(|child| !child_family.children.contains(child))
                })
                .choose(&mut rng)
                .unwrap();

            let other_children = attributions.get_mut(target).unwrap();

            let exchange_at = other_children
                .iter()
                .enumerate()
                .filter(|(_, child)| !child_family.children.contains(child))
                .choose(&mut rng)
                .unwrap()
                .0;

            let other_child = other_children[exchange_at];
            other_children[exchange_at] = child;

            attributions
                .get_mut(child_family)
                .unwrap()
                .push(other_child)
        }
    }
}

fn send_emails(
    families: &[Family],
    parent_attributions: HashMap<&Family, &Family>,
    child_attributions: HashMap<&Family, Vec<&str>>,
) -> Result<(), Box<dyn std::error::Error>> {
    for family in families {
        let parent_attribution = parent_attributions.get(family).unwrap();
        let children_attribution = child_attributions.get(family).unwrap();

        send_email(family, parent_attribution, children_attribution)?;
        println!(
            "{} : {}{}",
            family.name,
            parent_attribution.name,
            ComplementaryNameList(children_attribution)
        );
    }

    Ok(())
}

fn send_email(
    from: &Family,
    to_parent: &Family,
    to_children: &[&str],
) -> Result<(), Box<dyn std::error::Error>> {
    // Text inspired from https://github.com/fredericsureau/secret-santa
    let message = format!(
        "Ho ! Ho ! Ho !

Bonjour {mail_receiver} !

Cette année j'ai eu la chance d'échapper à la grippe de l'année dernière,
cependant j'ai pris un peu de retard dans mon travail. Comme je sais que dans
votre famille vous êtes toujours prêts à aider et que vous aimez beaucoup faire
les magasins, j'ai demandé à mes lutins de confectionner un petit logiciel pour
répartir les rôles entre vous sans que personne ne sache qui offre à qui.
Eh oui, c'est qu'ils se mettent à l'informatique mes lutins !

Je serais donc ravi si vous pouviez m'aider pour offrir de beaux cadeaux à
{gift_receiver}{child_receivers}.

Pour le budget je pense qu'environ 30€ pour les enfants et 50€ pour les grands
enfants sera raisonnable.

Comme je n'ai pas entièrement confiance en mes lutins, j'apprécierais une
réponse à ce mail *sans les noms des personnes qui vous ont été attribuées*,
juste pour être sûr.

Ah oui, aussi, envoyez-moi vite vos listes sur
<insert Nextcloud shared folder here> !

Allez ! Bybye !

Le Père-Noël.",
        mail_receiver = from.name,
        gift_receiver = to_parent.name,
        child_receivers = ComplementaryNameList(to_children)
    );

    let email = EmailBuilder::new()
        .to((from.email, from.name))
        .from(("papa.noel@guichard.top", "Le Père Noël"))
        .subject("[Confidentiel] Message du Père-Noël")
        .text(message)
        .build()
        .unwrap()
        .into();

    let mut mailer = SmtpClient::new_simple("smtp.server.org")?
        .credentials(Credentials::new(
            "santa-claus@example.com".to_string(),
            "password".to_string(),
        ))
        .smtp_utf8(true)
        .authentication_mechanism(Mechanism::Plain)
        .connection_reuse(ConnectionReuseParameters::ReuseUnlimited)
        .transport();
    mailer.send(email)?;

    Ok(())
}
